package com.mgmtp.services.helloworld

import com.mgmtp.helloworld.Application
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.util.LinkedMultiValueMap
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@ContextConfiguration(classes = Application)
@SpringBootTest(webEnvironment = RANDOM_PORT)
class ApplicationTests extends Specification {

	@Autowired
	private TestRestTemplate restTemplate

	def 'GET on hello-world service' () {
		setup:
		def response = sendRequest HttpMethod.GET, '/hello-world'

		expect:
		response
		response.statusCode == HttpStatus.OK
		response.hasBody()
		response.body.contains 'Hello world from '
	}

	@Unroll('GET on hello-world service accepting "#mimeType" -> #expectedStatusCode')
	def 'GET on hello-world service accepting mime type' () {
		setup:
		def response = sendRequest HttpMethod.GET, '/hello-world', [Accept: mimeType]

		expect:
		response
		response.statusCode == expectedStatusCode

		where:
		mimeType			|| expectedStatusCode
		'application/json'	|| HttpStatus.OK
		'text/plain'		|| HttpStatus.NOT_ACCEPTABLE
	}

	def 'POST on hello world service' () {
		setup:
		def response = sendRequest HttpMethod.POST, '/hello-world'

		expect:
		response
		response.statusCode == HttpStatus.METHOD_NOT_ALLOWED
	}

	ResponseEntity<String> sendRequest(HttpMethod httpMethod, String url, Map httpHeaders = [:]) {
		httpHeaders = new LinkedMultiValueMap(httpHeaders.collectEntries([:]) { k, v -> [(k): [v]]})

		restTemplate.exchange(url, httpMethod, new HttpEntity(httpHeaders), String)
	}
}

package com.mgmtp.helloworld

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RestController
import java.net.InetAddress

@RestController("/")
class HelloWorldEndpoint {

    class Message(val message: String)

	@RequestMapping(
		method   = arrayOf( GET ),
		path     = arrayOf( "hello-world" ),
		produces = arrayOf( "application/json;charset=UTF-8" )
	)
    fun helloWorld() = Message("Hello world from $hostInfo")

    // internal helpers -----------------------------------------------------------------------

    private val osInfo   by lazy { syspropsFor("os.name", "os.version", "os.arch") }
    private val javaInfo by lazy { syspropsFor("java.version", "java.vendor") }
    private val hostInfo by lazy { "$osInfo (Java $javaInfo) on ${hostname()}" }

    private fun syspropsFor(vararg keys: String) =
        keys.map { System.getProperty(it) } .joinToString(" ")

	private fun hostname(): String {
        return try {
            InetAddress.getLocalHost().toString()
        }
        catch (ignored: Throwable) {
            return try {
                Runtime.getRuntime().exec("hostname").inputStream.toString()
            }
            catch (ignored2: Throwable) {
                "n.a."
            }
        }
	}

}
# 'Hello World' Service using Kotlin

This project builds a simple _hello world_ service exposing a single HTTP service endpoint `hello-world` on port `4223`.

It is based on the [SpringBoot](https://spring.io/guides/gs/spring-boot/) framework and is written in [Kotlin](https://kotlinlang.org/).


## How to build it

The service application is a single JAR file built with the [Gradle Build Tool](https://gradle.org/).

To build the JAR file run the following command

    $ ./gradlew build
    

### The Gradle Wrapper

`gradlew` (or `gradlew.bat` under Windows) allows to run Gradle without having to install it manually:
it will be downloaded and installed when calling the wrapper for the first time. The [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) installs the Gradle version
required to build this project. The wrapper is located in the directory `gradle/wrapper` and has to be checked into VCS.


## How to run it

To run the service simply pass it to the Java 8 runtime executable:

    $ java -jar build/libs/hello-world-*.jar

Once the service is up, the one and only endpoint can be called:
 
    $ curl localhost:4223/hello-world

The result returned in the HTTP body should be something like this:

    {"message":"Hello world from Mac OS X 10.12.6 x86_64 (Java 1.8.0_144 Oracle Corporation), on marcusonb/192.168.178.20"}
